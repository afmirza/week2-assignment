import UIKit

public class Node<T> {
    var value: T
    var next: Node?
    weak var previous: Node?
    init(value: T) {
        self.value = value
        
    }
}
//
extension Node: CustomStringConvertible {
    public var description: String {
      let  text = "[\(value)]"

        return  text
    }
}

public class DoublyLinkedList<T> {

    fileprivate var head: Node<T>?
    private var tail: Node<T>?

    public var isEmpty: Bool {
        return head == nil
    }

    public var first: Node<T>? {
        return head
    }

    public var last: Node<T>? {
        guard var node = head else
        {
            return nil
        }

        while let next = node.next{
            node = next
        }
        return node
    }

    public func add (_ value: T){
        let newNode = Node(value: value)

        if let lastNode = last {
            newNode.previous = lastNode
            lastNode.next = newNode
        }
        else{
            head = newNode
        }

    }

    public var length: Int{
        guard var node = head else {
            return 0
        }
        var count = 1
        while let next = node.next {
            node = next
            count += 1
        }
        return count
    }

    public func node (at index: Int) -> Node<T>? {
        if index == 0 {
          return head!
        } else {
          var node = head!.next
          for _ in 1..<index {
            node = node?.next
            if node == nil { //(*1)
              break
            }
          }
          return node!
        }
    }

    public func remove(node: Node<T>) -> T {
      let prev = node.previous
      let next = node.next

      if let prev = prev {
        prev.next = next
      } else {
        head = next
      }
      next?.previous = prev

      node.previous = nil
      node.next = nil
      return node.value
    }



}
//
//// Customized printing DoublyLinkedList
//

extension DoublyLinkedList: CustomStringConvertible {
  
  public var description: String {
    
    var text = "H-"
    var node = head
    
    while node != nil {
      text += "\(node!.description)"
      node = node!.next
      if node != nil { text += "-" }
    }
    // 5
    return text + "-T"
  }
}


//        // Code Testing
//// Customized printing for Node
//var testNodePrint = Node(value: "Mercedes")
//print(testNodePrint)

var listOfCarModels = DoublyLinkedList<String>()
// checking if list is empty
listOfCarModels.isEmpty
print(listOfCarModels)
// getting first item from list
listOfCarModels.first

// adding item to the list
listOfCarModels.add("BMW")
listOfCarModels.add("Toyota")
listOfCarModels.add("Suzuki")
listOfCarModels.add("Honda")
listOfCarModels.add("Jaguar")
listOfCarModels.add("Mercedes")
// getting last item from list
listOfCarModels.last!

// testing length property
listOfCarModels.length

// getting node by index
listOfCarModels.node(at: 0)!.value
listOfCarModels.node(at: 1)!.value
 // deleting node
var c = listOfCarModels.node(at: 2)
listOfCarModels.remove(node: c!)
// checking list length after node deletion
listOfCarModels.length

// printing list with custom format
print(listOfCarModels)

// Implementing Stack using Doublylinked list

class Stack<T> {
    private var head: Node<T>?

    public var isEmpty: Bool{
        return head == nil
    }

    public var size: Int{
        var count = 0
        var current = head
        while (current != nil) {
            current = current?.next
            count += 1
        }
        return count
    }

     public func push (_ value: T){
            let newNode = Node(value: value)
            newNode.previous = nil
            if let headNode = head {
                newNode.next = headNode
                head = newNode
            }
            else{
                head = newNode
            }

        }

    func pop() -> T? {


        if let headNode = head {
            let node = headNode
            node.previous = nil
            head = headNode.next
            node.next = nil
            return node.value
        }
        return nil
    }

    var  peek: Node<T>? {
//        head?.previous = nil
        return head

    }

}

// Stack extension for custom printing

extension Stack: CustomStringConvertible {
    public var description: String{
        var text = "["
        var node = head
        while node != nil {
            text += "\(node!.value)"
            node = node!.next
            if node != nil {text += ", "}
        }
        return text + "]"
    }
}

// Alternate approach to stack implementation using Doubly Linked List

class Stack2<T>
{
    let myItems = DoublyLinkedList<T>()
    
    var size: Int {
        get { return myItems.length }
    }
    var peek: Node<T>? {
        get { return myItems.last }
    }
    func push(_ v: T) {
        myItems.add(v)
    }
    
    func pop() -> Any? {
        return myItems.remove(node: myItems.last!)
    }


}

// Stack2 extension for custom printing

extension Stack2: CustomStringConvertible {
    public var description: String{
        var text = "["
        var node = myItems.last
        while node != nil {
            text += "\(node!.value)"
            node = node!.previous
            if node != nil {text += ", "}
        }
        return text + "]"
    }
}

// testing Stack (First Implementation) isEmpty, push, pop, peek, size and custom printing
var testStack = Stack<Int>()
testStack.isEmpty
testStack.push(1)
testStack.push(2)
testStack.push(3)
testStack.push(4)
print(testStack)
testStack.size
testStack.peek
testStack.pop()
testStack.peek
testStack.size
print(testStack)

// testing Stack2 (Alternate implementation) push, pop, peek, size and custom printing
var testStack2 = Stack2<Int>()
testStack2.push(1)
testStack2.push(2)
testStack2.push(3)
testStack2.push(4)
print(testStack2)
testStack2.size
testStack2.peek
testStack2.pop()
testStack2.peek
testStack2.size
print(testStack2)

